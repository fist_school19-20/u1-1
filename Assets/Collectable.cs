﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public int id;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharController>() != null)
        {
            other.GetComponent<CharController>()
                .ChangeMats(id, 1);
            Destroy(gameObject);
        }
    }
}
