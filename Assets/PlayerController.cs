﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //private
    public GameObject terrain;
    public GameObject finish;
    public Text label;
    public Text scoreText;

    bool isWin = false;
    int score = 0;
    int total = 0;

    void Start()
    {
#warning gamemanager!
        //DontDestroyOnLoad(gameObject);
        Teapot[] teapots = FindObjectsOfType<Teapot>();
        for (int i = 0; i < teapots.Length; i++)
        {
            total += teapots[i].cost;
        }
        ChangeScore(0);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (isWin != true && collision.collider.gameObject == terrain)
        {
            print("game over");
            isWin = true;
            label.enabled = true;
            Destroy(gameObject);
        }
 
    }
    void OnTriggerEnter(Collider other)
    {
        if (!isWin && other.gameObject == finish)
        {
            print("you win");
            label.text = "you win";
            label.color = Color.green;
            label.enabled = true;
            isWin = true;

            FindObjectOfType<GameManager>().LoadLevel(
                SceneManager.GetActiveScene().buildIndex + 1
                );

        }
    }
    // void пустой, формальный результат


    /// ОБЪЯВЛЕНИЕ МЕТОДА:
    // модификатор доступа
    // тип возвращ знач
    // имя метода CamelCase, с глаголом
    // (набор входных аргументов)
    // { блок операций; тело метода }
    public void ChangeScore(int value)
    {
        score += value; // обновл данных
        scoreText.text = "Score: " + score +" / " + total;  // обновл UI
    }
}
