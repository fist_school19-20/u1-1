﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class CharController : MonoBehaviour
{
    public UIManager uim;
    public Sprite[] sprites;
    public GameObject slotPrefab;
    public RectTransform content;
    FirstPersonController fpsc;
    int selectedBlock = 0;
    int[] resources;
    int[] blocks;
    public Transform marker;
    public LandscapeGenerator lg;
    public Text hint;
    public RectTransform qp;
    public RectTransform ip;
    public RectTransform selector;
    Camera cam;
    RaycastHit hit = new RaycastHit();
    public Transform weaponContainer;
    void Start()
    {
        fpsc = GetComponent<FirstPersonController>();
        cam = GetComponentInChildren<Camera>();
        resources = new int[5];
        blocks = new int[5];
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            bool target = !ip.gameObject.activeInHierarchy;
            ip.gameObject.SetActive(target);
            fpsc.enabled = !target;
            Cursor.visible = target;

            Time.timeScale = target ? 0 : 1; // тернарная операция
            Cursor.lockState = target ? CursorLockMode.None : CursorLockMode.Locked;
            hint.enabled = !target;

            if (target)
            {
                for (int i = 0; i < resources.Length; i++) // Тип ресурса
                {
                    for (int j = 0; j < resources[i]; j++) // количество i-ресурса
                    {
                        GameObject s = GameObject.Instantiate(
                            slotPrefab,
                            content
                            );
                        s.transform.GetChild(0)
                            .GetComponent<Image>()
                            .sprite = sprites[i];
                        s.transform.GetChild(0)
                            .GetComponent<Resource>()
                            .Id = i;
                    }
                }
            }
            else
            {
                foreach(Transform t in content)
                {
                    Destroy(t.gameObject);
                }

                GameManager.Instance().hintPanel.gameObject.SetActive(false);
                GameManager.Instance().hintText.text = "";

                uim.ClearTable();
            }

            int n = content.childCount;
            int columns = (int)((content.rect.size.x - 4) / 68);
            int rows = (n / columns) + 1;

            content.sizeDelta = new Vector2(
                content.sizeDelta.x,
                rows * 64 + (rows + 1) * 4
            );

            
        }
        if (ip.gameObject.activeInHierarchy)
            return;

        // transform.forward, right, up
        Physics.Raycast(
            cam.transform.position, // коорд начала луча
            cam.transform.forward, // вектор направления луча
            out hit, //
            5.0f // расстояние взаимодействия
            );
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.
                GetComponent<Weapon>() != null)
            {
                hint.text = hit.collider.gameObject.
                    GetComponent<Weapon>().Name;
            }
            else
            {
                hint.text = hit.collider.gameObject.name;
                hint.text += "\n" + 
                    hit.collider.gameObject.GetComponent<HP>()?.GetHealth();
            }
            if (hit.collider.tag == "Block")
            {
                marker.GetComponent<MeshRenderer>().enabled = true;
                marker.position = hit.collider.transform.position
                    + hit.normal;
            }
            else
            {
                marker.GetComponent<MeshRenderer>().enabled = false;
            }

        }
        else
        {
            hint.text = "";
            marker.GetComponent<MeshRenderer>().enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            
            // object -> bool => true
            // null   -> bool => false
            if (hit.collider && hit.collider.gameObject.GetComponent<Weapon>())
            {
                DropWeapon();

                hit.collider.transform.SetParent(weaponContainer);
                weaponContainer.GetChild(0).localPosition = Vector3.zero;
                weaponContainer.GetChild(0).localRotation = Quaternion.identity;
                weaponContainer.GetChild(0).GetComponent<Animator>().enabled = true;
                weaponContainer.GetChild(0).GetComponent<Rigidbody>().isKinematic = true;
            }
        }

        if (Input.GetMouseButtonDown(0) && weaponContainer.childCount > 0)
        {
            weaponContainer.GetChild(0)
                .GetComponent<Animator>()
                .SetTrigger("attack");
            if (hit.collider != null
                && hit.collider.gameObject.GetComponent<HP>() != null)
            {
                hit.collider.gameObject.GetComponent<HP>().GetDamage(
                    weaponContainer.GetChild(0)
                    .GetComponent<Weapon>().Damage);
            }
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            DropWeapon();
        }
        if (Input.GetMouseButtonDown(1))
        {
            //build block
            GameObject.Instantiate(
                lg.prefabs[selectedBlock],
                marker.position,
                marker.rotation
                );
        }
        if (Input.GetKeyDown(KeyCode.Alpha1)) selectedBlock = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2)) selectedBlock = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3)) selectedBlock = 2;
        if (Input.GetKeyDown(KeyCode.Alpha4)) selectedBlock = 3;
        if (Input.GetKeyDown(KeyCode.Alpha5)) selectedBlock = 4;
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0) selectedBlock++;
            if (Input.GetAxis("Mouse ScrollWheel") < 0) selectedBlock--;

            int k = lg.prefabs.Length - 1;
            selectedBlock = (k + selectedBlock) % k;
        }

        selector.SetParent(qp.GetChild(6 + selectedBlock));
        selector.localPosition = Vector3.zero;
    }
    void DropWeapon()
    {
        if (weaponContainer.childCount == 0)
            return;

        weaponContainer.GetChild(0)
            .GetComponent<Rigidbody>().isKinematic = false;
        weaponContainer.GetChild(0).GetComponent<Animator>().enabled = false;
        weaponContainer.GetChild(0).SetParent(null);
    }
    public void ChangeMats(int id, int value)
    {
        resources[id] += value;
        qp.GetChild(id)
            .GetComponentInChildren<Text>()
            .text = resources[id].ToString();
    }
    public void ChangeBlocks(int id, int value)
    {
        blocks[id] += value;
        qp.GetChild(6 + id)
            .GetComponentInChildren<Text>()
            .text = blocks[id].ToString();
    }
}
