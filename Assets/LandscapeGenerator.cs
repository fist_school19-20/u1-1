﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LandscapeGenerator : MonoBehaviour
{
    [Range(0,1)] public float k;
    public int size;
    public GameObject[] prefabs;
    void Start()
    {
        int seed = Random.Range(0, 16); // зерно генерации
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                int y = (int)(size * k * 
                    Mathf.PerlinNoise(seed + i / (1f*size), seed + j / (1f*size)));
                //for (int y = Random.Range(0, 4); y >= 0; y--)
                //{
                GameObject.Instantiate(
                    prefabs[(int)(y * prefabs.Length / (size*k))], // об. оригинал
                    new Vector3(i, y, j), // pos 0,0,0
                    Quaternion.identity, // rot 0,0,0
                    transform // куда вложить копию
                );
                //}

            }
        }
        GetComponent<NavMeshSurface>().BuildNavMesh();
        /*
        foreach (Transform block in transform)
        {
            block.GetComponent<NavMeshSurface>().BuildNavMesh();
        }
        */
    }
}
