﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : InventoryItem
{
    public int Damage;
    public int CooldownTime;
}
