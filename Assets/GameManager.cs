﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    #region Singleton
    private static GameManager _instance; // = null
    public static GameManager Instance() { return _instance; }
    
    void Awake()
    {
        if (_instance == null)
            _instance = this;
        DontDestroyOnLoad(gameObject);
    }
    #endregion Singleton

    public int PlayerScore;
    public UIManager uiManager;
    public CharController player;
    public Canvas HUD;

    public RectTransform hintPanel;
    public Text hintText;

    public void LoadLevel(int index)
    {
        if (index == SceneManager.sceneCountInBuildSettings - 1)
        {
            //если посл уровень
            SceneManager.LoadScene(0);
        }
        else
        {
            SceneManager.LoadScene(index);
        }
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
