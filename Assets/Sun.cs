﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{
    [Range(1, 20)][SerializeField] float speed;
    void Update()
    {
        transform.Rotate(Vector3.left * Time.deltaTime * speed);
    }
}
