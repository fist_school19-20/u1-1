﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public RectTransform[] craftTable;
    public void Craft()
    {
        if (craftTable[0].childCount == 0)
        {
            print("Error");
            return;
        }
        int id = craftTable[0].GetChild(0).GetComponent<Resource>().Id;
        foreach (RectTransform slot in craftTable)
        {
            if (slot.childCount == 0 ||
                slot.GetChild(0).GetComponent<Resource>().Id != id)
            {
                print("Error");
                return;
            }
        }
        print("Crafted");
        GameManager.Instance().player.ChangeMats(id, -4);
        GameManager.Instance().player.ChangeBlocks(id, 1);
        ClearTable();
    }
    public void ClearTable()
    {
        foreach (RectTransform slot in craftTable)
        {
            if (slot.childCount > 0)
                Destroy(slot.GetChild(0).gameObject);
        }
    }
    private void Update()
    {
        GameManager.Instance().hintPanel.position = Input.mousePosition;
    }
}
