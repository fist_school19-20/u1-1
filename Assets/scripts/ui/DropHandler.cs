﻿using UnityEngine;
using UnityEngine.EventSystems;
public class DropHandler : MonoBehaviour,
    IDropHandler
{
    public void OnDrop(PointerEventData data)
    {
        DragHandler item = data.pointerDrag //GameObj
            .GetComponent<DragHandler>();
        if (item != null)
        {
            if (transform.childCount > 0)
            { 
                transform.GetChild(0).SetParent(item.startParent);
                item.startParent
                    .GetChild(0).localPosition = Vector3.zero;
            }
            item.startParent = transform;
        }
    }
}
