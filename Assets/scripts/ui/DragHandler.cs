﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour,
    IDragHandler, 
    IBeginDragHandler, 
    IEndDragHandler
{
    public Transform startParent;
    public void OnBeginDrag(PointerEventData data)
    {
        startParent = transform.parent;
        transform
            //.SetParent(transform.parent.parent.parent.parent.parent);
            .SetParent(
                GetComponentInParent<UIManager>().transform
            );
        transform.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }
    public void OnDrag(PointerEventData data)
    {
        transform.position = data.position;
    }
    public void OnEndDrag(PointerEventData data)
    {
        transform.SetParent(startParent);
        transform.localPosition = Vector3.zero;
        transform.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
}