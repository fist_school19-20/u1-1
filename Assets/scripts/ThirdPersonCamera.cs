﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    const float Y_MAX = 80f; //константа
    const float Y_MIN = 10f;

    public Transform target;
    public float distance = 5.0f;
    public float sensX = 4;
    public float sensY = 4;
    public float distMin = 1;
    public float distMax = 10;

    float curX = 0;
    float curY = 0;
    void Update()
    {       
        curX += Input.GetAxis("Mouse X") * sensX;
        curY += Input.GetAxis("Mouse Y") * sensY;
        curY = Mathf.Clamp(curY, Y_MIN, Y_MAX);

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
            distance -= Mathf.Sign(Input.GetAxis("Mouse ScrollWheel"));
        distance = Mathf.Clamp(distance, distMin, distMax);
    }
    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rot = Quaternion.Euler(curY, curX, 0);
        transform.position = target.position + rot * dir;
        transform.LookAt(target);
    }
}
