﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryItem : MonoBehaviour,
    IPointerEnterHandler,
    IPointerExitHandler
{
    public int Id;
    public string Name;
    public Sprite Icon;
    public int Weight;

    public void OnPointerEnter(PointerEventData data)
    {
        GameManager.Instance().hintPanel.gameObject.SetActive(true);
        GameManager.Instance().hintText.text = Name;
    }
    public void OnPointerExit(PointerEventData data)
    {
        GameManager.Instance().hintPanel.gameObject.SetActive(false);
        GameManager.Instance().hintText.text = "";
    }
}
