﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : MonoBehaviour
{
    [SerializeField] int maxHealth;
    int currentHealth;
    [SerializeField] GameObject destroyed;

    private void Start()
    {
        currentHealth = maxHealth;
    }
    public void GetDamage(int dmg)
    {
        currentHealth -= dmg;
        if (currentHealth <= 0)
        {
            GameObject.Instantiate(
                destroyed,
                transform.position,
                transform.rotation,
                transform.parent
                );
            Destroy(gameObject);
        }
    }
    public string GetHealth()
    {
        return currentHealth + " / " + maxHealth;
    }
}
