﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teapot : MonoBehaviour
{
    public int cost;

    void OnTriggerEnter(Collider other)
    {
        // GetComponent<T>()
        if (other.GetComponent<PlayerController>() != null)
        {
            Destroy(GetComponent<SphereCollider>());
            //GetComponent<SphereCollider>().enabled = false;
            // ВЫЗОВ МЕТОДА ChangeScore:
            other
                .GetComponent<PlayerController>()
                .ChangeScore(cost);
            GetComponent<Animator>().Play("collect");
            Destroy(gameObject, 1f);
        }
    }
}
