﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Quaternion rot = transform.rotation;
        rot.x -= h * 0.02f;
        rot.z -= v * 0.02f;
        transform.rotation = rot;
    }
}
