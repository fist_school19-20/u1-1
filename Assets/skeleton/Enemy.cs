﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    Animator animator;
    CharController player = null; // память врага
    NavMeshAgent agent;
    void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    void FixedUpdate()
    {        
        if (player != null)
        {
            agent.SetDestination(player.transform.position);
            agent.isStopped = false;
            if (Vector3.Magnitude(transform.position - player.transform.position) <= 2.5f)
            {
                // attack
                animator.SetInteger("state", 3);
            }
            else
            {
                // run
                // двигать вперед, к игроку
                animator.SetInteger("state", 1);
            }
        }
        else
        {
            //idle
            agent.isStopped = true;
            animator.SetInteger("state", 0);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharController>() != null)
        {
            player = other.GetComponent<CharController>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CharController>() != null)
            player = null;
    }
}
